﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Models;

namespace ShareTheCosts.Data
{
    public interface IShareTheCostsContext
    {
        DbSet<Attendee> Attendees { get; set; }
        DbSet<Choice> Choices { get; set; }
        DbSet<EventAttendee> EventAttendees { get; set; }
        DbSet<Event> Events { get; set; }
        DbSet<ExpenseAttendee> ExpenseAttendees { get; set; }
        DbSet<Expense> Expenses { get; set; }
        DbSet<Payment> Payments { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}