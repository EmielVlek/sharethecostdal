﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ShareTheCosts.Data
{
    public class ShareTheCostsContext : IdentityDbContext<User>, IShareTheCostsContext
    {
        public ShareTheCostsContext(DbContextOptions<ShareTheCostsContext> options)
            : base(options)
        {
        }

        // Deze is niet meer hiet te vinden omdat dit al in IdentityDBContext<User> is gedefineerd
        //public DbSet<ShareTheCosts.Models.User> Users { get; set; }

        public DbSet<ShareTheCosts.Models.Attendee> Attendees { get; set; }

        public DbSet<ShareTheCosts.Models.Event> Events { get; set; }

        public DbSet<ShareTheCosts.Models.Expense> Expenses { get; set; }

        public DbSet<ShareTheCosts.Models.Payment> Payments { get; set; }

        public DbSet<ShareTheCosts.Models.Choice> Choices { get; set; }

        public DbSet<ShareTheCosts.Models.EventAttendee> EventAttendees { get; set; }

        public DbSet<ShareTheCosts.Models.ExpenseAttendee> ExpenseAttendees { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Event>().Property<bool>("IsDeleted");
            modelBuilder.Entity<Event>().HasQueryFilter(e => EF.Property<bool>(e, "IsDeleted") == false);
        }

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            var AddedEntities = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added)
                .ToList();
            AddedEntities.ForEach(e =>
            {
                DateTime now = DateTime.Now;
                e.Property("CreatedDate").CurrentValue = now;
                e.Property("ModifiedDate").CurrentValue = now;
            });

            var EditedEntities = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified)
                .ToList();
            EditedEntities.ForEach(e =>
            {
                e.Property("ModifiedDate").CurrentValue = DateTime.Now;
            });
            return base.SaveChanges();
        }


        /// <summary>
        /// Override of the SaveChangesAsynch Method to facilitate BaseEntity DateTime interactions
        /// </summary>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            UpdateSoftDeleteStatuses();
            var AddedEntities = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added)
                .ToList();
            AddedEntities.ForEach(e =>
            {
                DateTime now = DateTime.Now;
                e.Property("CreatedDate").CurrentValue = now;
                e.Property("ModifiedDate").CurrentValue = now;
            });

            var EditedEntities = ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Modified)
                .ToList();
            EditedEntities.ForEach(e =>
            {
                e.Property("ModifiedDate").CurrentValue = DateTime.Now;
            });

            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }

            }
        }
    }
}