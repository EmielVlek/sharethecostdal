﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareTheCosts.Migrations
{
    public partial class patch_035_No_double_payer_ids : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Attendees_PayerId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Attendees_PayerId",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Attendees_ReceiverId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "PayerAttendeeId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "ReceiverAttendeeId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "PayerAttendeeId",
                table: "Expenses");

            migrationBuilder.AlterColumn<int>(
                name: "ReceiverId",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PayerId",
                table: "Payments",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PayerId",
                table: "Expenses",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Attendees_PayerId",
                table: "Expenses",
                column: "PayerId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Attendees_PayerId",
                table: "Payments",
                column: "PayerId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Attendees_ReceiverId",
                table: "Payments",
                column: "ReceiverId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Attendees_PayerId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Attendees_PayerId",
                table: "Payments");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Attendees_ReceiverId",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "ReceiverId",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PayerId",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "PayerAttendeeId",
                table: "Payments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReceiverAttendeeId",
                table: "Payments",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "PayerId",
                table: "Expenses",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "PayerAttendeeId",
                table: "Expenses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Attendees_PayerId",
                table: "Expenses",
                column: "PayerId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Attendees_PayerId",
                table: "Payments",
                column: "PayerId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Attendees_ReceiverId",
                table: "Payments",
                column: "ReceiverId",
                principalTable: "Attendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
