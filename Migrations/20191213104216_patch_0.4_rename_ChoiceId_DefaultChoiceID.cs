﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShareTheCosts.Migrations
{
    public partial class patch_04_rename_ChoiceId_DefaultChoiceID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attendees_Choices_ChoiceId",
                table: "Attendees");

            migrationBuilder.DropIndex(
                name: "IX_Attendees_ChoiceId",
                table: "Attendees");

            migrationBuilder.DropColumn(
                name: "ChoiceId",
                table: "Attendees");

            migrationBuilder.AddColumn<int>(
                name: "DefaultChoiceId",
                table: "Attendees",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateIndex(
                name: "IX_Attendees_DefaultChoiceId",
                table: "Attendees",
                column: "DefaultChoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attendees_Choices_DefaultChoiceId",
                table: "Attendees",
                column: "DefaultChoiceId",
                principalTable: "Choices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attendees_Choices_DefaultChoiceId",
                table: "Attendees");

            migrationBuilder.DropIndex(
                name: "IX_Attendees_DefaultChoiceId",
                table: "Attendees");

            migrationBuilder.DropColumn(
                name: "DefaultChoiceId",
                table: "Attendees");

            migrationBuilder.AddColumn<int>(
                name: "ChoiceId",
                table: "Attendees",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Attendees_ChoiceId",
                table: "Attendees",
                column: "ChoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attendees_Choices_ChoiceId",
                table: "Attendees",
                column: "ChoiceId",
                principalTable: "Choices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
