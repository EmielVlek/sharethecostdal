﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class Event : BaseEntity
    {
        public Event()
        {
        }


        [Required]
        [MaxLength(100)]
        [DataMember]
        public string EventName { get; set; }

#nullable enable
        [MaxLength(2000)]
        [DataMember]
        public string? EventDescription { get; set; }
#nullable disable

        [Required]
        [DataMember]
        public string UserId { get; set; }
        [XmlIgnore]
        public User User { get; set; }


        [XmlIgnore]
        public ICollection<Expense> Expenses { get; set; }


        [XmlIgnore]
        public ICollection<Payment> Payments { get; set; }

        [XmlIgnore]
        public ICollection<IOU> IOUs { get; set; }


        [DataMember]
        public ICollection<EventAttendee> EventAttendees { get; set; }


    }
}
