﻿using System.ComponentModel.DataAnnotations;

namespace ShareTheCosts.Models
{
    public class UserInfo
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
