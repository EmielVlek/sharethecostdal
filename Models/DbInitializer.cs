﻿using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using System.Linq;

namespace ShareTheCosts.Models
{
    public class DbInitializer
    {
        public static void Initialize(ShareTheCostsContext context)
        {
            context.Database.Migrate();

            // Look for any Choices.
            if (context.Choices.Any())
            {
                return;   // DB has been seeded
            }

            context.Choices.AddRange(
                new Choice
                {
                    ChoiceString = "equal percentage"
                },
                new Choice
                {
                    ChoiceString = "amount"
                },
                new Choice
                {
                    ChoiceString = "amount and equal percentage"
                },
                new Choice
                {
                    ChoiceString = "percentage"
                },
                new Choice
                {
                    ChoiceString = "amount and percentage"
                }
            );
            context.SaveChanges();
        }
    }
}
