﻿using System;

namespace ShareTheCosts.Models
{
    public interface IDateEntity
    {
        DateTime CreatedDate { get; set; }
        DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}