﻿namespace ShareTheCosts.Models.Interfaces
{
    public interface IIdEntity : IDateEntity
    {
        public int Id { get; set; }
    }
}
