﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ShareTheCosts.Models
{
    public class Choice : BaseEntity
    {
        public Choice()
        {
        }


        [MaxLength(50)]
        [DataMember]
        public string ChoiceString { get; set; }


        public ICollection<Attendee> Attendees { get; set; }


        public ICollection<EventAttendee> EventAttendees { get; set; }


        public ICollection<ExpenseAttendee> ExpenseAttendees { get; set; }
    }
}
