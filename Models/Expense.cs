﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class Expense : BaseEntity
    {
        public Expense()
        {
        }


        [Required]
        [MaxLength(100)]
        [DataMember]
        public string ExpenseName { get; set; }


        [Required]
        [Column(TypeName = "decimal(10,2)")]
        [DataMember]
        public decimal Amount { get; set; }


        [MaxLength(500)]
        [DataMember]
        public string DescriptionWhere { get; set; }


        [DataMember]
        public DateTime PurchaseDate { get; set; }


        [Required]
        [DataMember]
        public int PayerId { get; set; }
        [XmlIgnore]
        public Attendee Payer { get; set; }
        

        [Required]
        [DataMember]
        public int EventId { get; set; }
        [XmlIgnore]
        public Event Event { get; set; }

        [DataMember]
        public ICollection<ExpenseAttendee> ExpenseAttendees { get; set; }
    }
}
