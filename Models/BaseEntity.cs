﻿using ShareTheCosts.Models.Interfaces;
using System;
using System.Runtime.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class BaseEntity : IIdEntity
    {
        [DataMember]
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
