﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ShareTheCosts.Models
{
    public class User : IdentityUser, IDateEntity
    {
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }


        public ICollection<Attendee> Attendees { get; set; }


        public ICollection<Event> Events { get; set; }
    }
}
