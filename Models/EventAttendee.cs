﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class EventAttendee : BaseEntity
    {
        public EventAttendee()
        {
        }

        public EventAttendee(Choice c, Event e, Attendee a)
        {
            Choice = c;
            Event = e;
            Attendee = a;
        }


        [Required]
        [DataMember]
        public int ChoiceId { get; set; }
        public Choice Choice { get; set; }


        [Required]
        [DataMember]
        public int EventId { get; set; }
        public Event Event { get; set; }


        [Required]
        [DataMember]
        public int AttendeeId { get; set; }
        [DataMember]
        public Attendee Attendee { get; set; }
    }
}
