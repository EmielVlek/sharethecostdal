﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class ExpenseAttendee : BaseEntity
    {
        public ExpenseAttendee()
        {
        }


        [DataMember]
        public int? ChoiceId { get; set; }
        public Choice Choice { get; set; }


        [Column(TypeName = "decimal(10,2)")]
        [DataMember]
        public decimal? OptionalSetAmount { get; set; }


        [Range(1,100)]
        [DataMember]
        public int? OptionalSetPercentage { get; set; }


        [DataMember]
        public int ExpenseId { get; set; }
        [XmlIgnore]
        public Expense Expense { get; set; }


        [DataMember]
        public int AttendeeId { get; set; }
        [DataMember]
        public Attendee Attendee { get; set; }
    }
}
