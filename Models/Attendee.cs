﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ShareTheCosts.Models
{
    public class Attendee : BaseEntity
    {
        public Attendee()
        {
        }

        public Attendee(string name, bool u, User user, Choice choice)
        {

            AttendeeName = name;
            IsUser = u;
            UserId = user.Id;
            User = user;
            DefaultChoiceId = choice.Id;
            DefaultChoice = choice;

        }


        [Required]
        [MaxLength(100)]
        [DataMember]
        public string AttendeeName { get; set; }


        public bool IsUser { get; set; }


        [Required]
        [DataMember]
        public int DefaultChoiceId { get; set; }
        [ForeignKey("DefaultChoiceId")]
        public Choice DefaultChoice { get; set; }

        [Required]
        [DataMember]
        public string UserId { get; set; }
        public virtual User User { get; set; }


        [InverseProperty("Payer")]
        public ICollection<Payment> PaidPayments { get; set; }
        [InverseProperty("Receiver")]
        public ICollection<Payment> ReceivedPayments { get; set; }


        [InverseProperty("Payer")]
        public ICollection<IOU> ToPayIOU { get; set; }
        [InverseProperty("Receiver")]
        public ICollection<IOU> ToReceiveIOU { get; set; }

        
        [InverseProperty("Payer")]
        public ICollection<Expense> Expenses { get; set; }

        
        public ICollection<EventAttendee> EventAttendees { get; set; }


        public ICollection<ExpenseAttendee> ExpenseAttendees { get; set; }
    }
}
