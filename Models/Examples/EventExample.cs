﻿using Swashbuckle.AspNetCore.Filters;
using System.Collections.Generic;

namespace ShareTheCosts.Models.Examples
{
    public class EventExample : IExamplesProvider<Event>
    {
        public Event GetExamples()
        {
            return new Event {
                Id = 2003,
                UserId = "d1b35d20-ceeb-4b24-a329-8beafb452506",
                EventName = "name-of-event",
                EventAttendees = new List<EventAttendee> {
                   new EventAttendee {
                       Id = 415,
                       AttendeeId = 1,
                       EventId = 2003,
                       Attendee = new Attendee {
                           AttendeeName = "Joe",
                           Id = 1,
                           UserId = "d1b35d20-ceeb-4b24-a329-8beafb452506",
                           DefaultChoiceId = 1
                       },
                       ChoiceId = 2 },
                   new EventAttendee {
                       Id = 416,
                       AttendeeId = 3,
                       EventId = 2003,
                       Attendee = new Attendee {
                           AttendeeName = "Eva",
                           Id = 5,
                           UserId = "d1b35d20-ceeb-4b24-a329-8beafb452506",
                           DefaultChoiceId = 1
                       },
                       ChoiceId = 1
                   }
               }
           };
        }
    }
}
