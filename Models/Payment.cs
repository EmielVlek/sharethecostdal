﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShareTheCosts.Models
{
    public class Payment : BaseEntity
    {
        public Payment()
        {
        }


        [Required]
        [Column(TypeName = "decimal(10,2)")]
        public decimal Amount { get; set; }


        [MaxLength(1000)]
        public string Remark { get; set; }

        
        [Required]
        public int EventId { get; set; }
        public Event Event { get; set; }
        

        [Required]
        public int? PayerId { get; set; }
        public Attendee Payer { get; set; }


        [Required]
        public int? ReceiverId { get; set; }
        public Attendee Receiver { get; set; }
    }
}
