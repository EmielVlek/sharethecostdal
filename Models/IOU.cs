﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ShareTheCosts.Models
{
    [DataContract]
    public class IOU : BaseEntity
    {
        public IOU() 
        {
        }

        public IOU(Attendee payer, Attendee receiver, decimal amount)
        {
            Payer = payer;
            Receiver = receiver;
            Amount = amount;
        }

        [DataMember]
        public Attendee Payer { get; set; }

        [DataMember]
        public Attendee Receiver { get; set; }


        [Column(TypeName = "decimal(10,2)")]
        public decimal Amount { get; set; }


        [XmlIgnore]
        public Event Event { get; set; }
    }
}
