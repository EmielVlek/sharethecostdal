﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareTheCosts.Controllers
{

    [Route("api/v1/IOU")]
    [ApiController]
    public class IOUController : ControllerBase
    {
        private readonly IOUMngr _iOUMngr;

        public IOUController(ShareTheCostsContext context)
        {
            _iOUMngr = IOUMngr.Create(context);
        }


        /// <summary>
        /// Get all the raw IOUinfo on a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/IOU/1
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>A collection of everything attendees owe eachother</returns>
        /// <response code="200">Succesfully returns the requested collection of IOU's</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}")]
        [ProducesResponseType(typeof(List<IOU>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<ICollection<IOU>>> GetIOU(int eventId)
        {
            try
            {
                List<IOU> result = await _iOUMngr.GetIOU(eventId);
                return new ActionResult<ICollection<IOU>>(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.InnerException.Message);
                throw;
            }
        }


        /// <summary>
        /// Get the least amount of transaction in a collection of IOU's of a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/IOU/1/optimized
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>A collection of everything attendees owe eachother</returns>
        /// <response code="200">Succesfully returns the requested collection of IOU's</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}/optimized")]
        [ProducesResponseType(typeof(List<IOU>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ICollection<IOU>> GetOptimizedIOU(int eventId)
        {
            try
            {
                return await _iOUMngr.GetOptimizedIOU(eventId);
            }
            catch
            {
                throw;
            }
        }

    }
}
