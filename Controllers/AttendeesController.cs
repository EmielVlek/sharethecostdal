﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/attendee")]
    [ApiController]
    public class AttendeesController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;

        public AttendeesController(ShareTheCostsContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Get all the Attendees currently stored in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/attendee
        ///
        /// </remarks>
        /// <returns>All attendees</returns>
        /// <response code="200">Returns a collection of all attendees</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Attendee>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Attendee>>> GetAllAttendees()
        {
            return await _context.Attendees.ToListAsync();
        }


        /// <summary>
        /// Get info on a specific attendee
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/attendee/1
        ///     
        /// </remarks>
        /// <param name="attendeeId"></param>
        /// <returns>One specific attendee</returns>
        /// <response code="200">Succesfully returns the requested attendee</response>
        /// <response code="404">If the requested attendee could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{attendeeId}")]
        [ProducesResponseType(typeof(Attendee), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Attendee>> GetAttendee(int attendeeId)
        {
            var attendee = await _context.Attendees.Include(a => a.EventAttendees).Include(a => a.ExpenseAttendees).FirstOrDefaultAsync(a => a.Id == attendeeId);

            if (attendee == null)
            {
                return NotFound();
            }

            return attendee;
        }


        /// <summary>
        /// Replace a current attendee with a new name and/or a new choice
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/v1/attendee/"id"
        ///     {
        ///         "attendeeName": "string",
        ///         "choiceId": 1
        ///     }
        /// </remarks>
        /// <param name="attendeeId"></param>
        /// <param name="attendee"></param>
        /// <returns>Nothing if it was a succes</returns>
        /// <response code="204">Returns no content on a succesful PUT</response>
        /// <response code="400">Could not match the given Attendee</response>
        /// <response code="404">Could not find the Attendee in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPut("{attendeeId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutAttendee(int attendeeId, Attendee attendee)
        {
            if (attendeeId != attendee.Id)
            {
                return BadRequest();
            }
            Attendee storedAttendee = await _context.Attendees.FindAsync(attendeeId);
            if (storedAttendee == null)
            {
                return NotFound();
            }
            storedAttendee.AttendeeName = attendee.AttendeeName;
            storedAttendee.DefaultChoiceId = attendee.DefaultChoiceId;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Add an Attendee to the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/v1/attendee
        ///     {
        ///         "attendeeName": "SomeNameString",
        ///         "choiceId": 3,
        ///         "UserId": "user id string"
        ///     }
        /// </remarks>
        /// <param name="attendee"></param>
        /// <returns>The created Attendee</returns>
        /// <response code="201">Returns the newly created Attendee</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Server Error</response>
        [HttpPost]
        [ProducesResponseType(typeof(Attendee), 201)]
        [ProducesResponseType(typeof(IDictionary<string,string>), 400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Attendee>> PostAttendee(Attendee attendee)
        {
            _context.Attendees.Add(attendee);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAttendee", new { attendeeId = attendee.Id }, attendee);
        }


        /// <summary>
        /// Remove an Attendee from the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /api/v1/attendee/1
        /// </remarks>
        /// <param name="attendeeId"></param>
        /// <returns>The deleted Attendee</returns>
        /// <response code="200">Returns the deleted Attendee</response>
        /// <response code="404">Could not find the Attendee</response>
        /// <response code="500">Server Error</response>
        [HttpDelete("{attendeeId}")]
        [ProducesResponseType(typeof(Attendee), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Attendee>> DeleteAttendee(int attendeeId)
        {
            var attendee = await _context.Attendees.FindAsync(attendeeId);
            if (attendee == null)
            {
                return NotFound();
            }

            #region Remove related Join tables
            var @eventAttendees = await _context.EventAttendees
                                        .Where(eva => eva.AttendeeId == attendeeId)
                                        .ToListAsync();
            foreach (var eva in @eventAttendees)
            {
                _context.EventAttendees.Remove(eva);
            }

            var @expenseAttendees = await _context.ExpenseAttendees
                            .Where(exa => exa.AttendeeId == attendeeId)
                            .ToListAsync();
            foreach (var exa in @expenseAttendees)
            {
                _context.ExpenseAttendees.Remove(exa);
            }

            #endregion

            _context.Attendees.Remove(attendee);

            await _context.SaveChangesAsync();

            return attendee;
        }

        //private bool AttendeeExists(int id)
        //{
        //    return _context.Attendees.Any(e => e.Id == id);
        //}
    }
}
