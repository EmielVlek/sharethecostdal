﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/choice")]
    [ApiController]
    public class ChoicesController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;

        public ChoicesController(ShareTheCostsContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Get all the available choices
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/choice
        ///     
        /// </remarks>
        /// <returns>A collection of all the choices</returns>
        /// <response code="200">Succesfully returns the requested collection</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Choice>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Choice>>> GetChoices()
        {
            return await _context.Choices.ToListAsync();
        }


        /// <summary>
        /// Get a specific choice
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/choice/"id"
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>The found choice</returns>
        /// <response code="200">Returns the found choice</response>
        /// <response code="404">Could not find the Attendee in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Choice), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Choice>> GetChoice(int id)
        {
            var choice = await _context.Choices.FindAsync(id);

            if (choice == null)
            {
                return NotFound();
            }

            return choice;
        }
    }
}
