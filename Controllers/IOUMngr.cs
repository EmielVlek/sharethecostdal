﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShareTheCosts.Controllers
{
    public class IOUMngr
    {
        private readonly ShareTheCostsContext _context;
        IOUMngr(ShareTheCostsContext context)
        {
            _context = context;
        }

        public static IOUMngr Create(ShareTheCostsContext context)
        {
            IOUMngr manager = new IOUMngr(context);
            return manager;
        }


        public async Task<List<IOU>> GetIOU(int eventId)
        {
            if (!EventExists(eventId))
            {
                throw (new Exception("Could not find the requested event in database"));
            }

            List<IOU> result;

            try
            {
                var debtCollections = await CreateDebtCollections(eventId);

                result = await CreateOptimizedIOUList(debtCollections);
            }
            catch
            {
                throw;
            }

            return result;
        }

        public async Task<ICollection<IOU>> GetOptimizedIOU(int eventId)
        {
            if (!EventExists(eventId))
            {
                throw(new Exception("Could not find the requested event in database"));
            }

            List<IOU> result;

            try
            {
                var debtCollections = await CreateDebtCollections(eventId);

                result = await CreateOptimizedIOUList(debtCollections);
            }
            catch
            {
                throw;
            }

            return result;
        }

        private async Task<IDictionary<string, Dictionary<int, decimal>>> CreateDebtCollections(int eventId)
        {
            Dictionary<int, decimal> debtIn = new Dictionary<int, decimal>();
            Dictionary<int, decimal> debtOut = new Dictionary<int, decimal>();

            List<Expense> expenses = await _context.Expenses.Where(e => e.EventId == eventId).Include(e => e.ExpenseAttendees).ToListAsync();
            List<Payment> payments = await _context.Payments.Where(p => p.EventId == eventId).ToListAsync();

            foreach (Expense expense in expenses)
            {
                foreach (ExpenseAttendee exa in expense.ExpenseAttendees)
                {
                    if (!debtIn.ContainsKey(exa.AttendeeId))
                    {
                        debtIn.Add(exa.AttendeeId, new decimal(0));
                    }

                    if (!debtOut.ContainsKey(exa.AttendeeId))
                    {
                        debtOut.Add(exa.AttendeeId, new decimal(0));
                    }
                }
            }
            foreach (Payment p in payments)
            {
                if (p.PayerId.HasValue && !debtOut.ContainsKey(p.PayerId.Value))
                {
                    debtOut.Add(p.PayerId.Value, new decimal(0));
                }
                if (p.ReceiverId.HasValue && !debtIn.ContainsKey(p.ReceiverId.Value))
                {
                    debtIn.Add(p.ReceiverId.Value, new decimal(0));
                }
            }


            //bool arb;
            // TODO Nu staan de 2 Collections klaar om ingevuld te worden
            //try
            //{
            //    foreach (Expense e in expenses)
            //    {
            //        List<ExpenseAttendee> exas = await _context.ExpenseAttendees.Where(exa => exa.ExpenseId == e.Id).ToListAsync();
            //        arb = await AddExpenseToIOU(debtIn, debtOut, e, exas);
            //    }
            //}
            //catch
            //{
            //    throw;
            //}

            //try
            //{
            //    foreach (Payment p in payments)
            //    {
            //        AddPayment(debtIn, debtOut, p);
            //    }
            //}
            //catch
            //{
            //    throw;
            //}


            Dictionary<string, Dictionary<int, decimal>> debtCollections = new Dictionary<string, Dictionary<int, decimal>>
            {
                { "In", debtIn },
                { "Out", debtOut }
            };

            //if (debtIn.Sum(item => item.Value) != debtOut.Sum(item => item.Value))
            //{
            //    throw new Exception("Something was wrong as the amount to be payed no longer matches what is expected to receive!");
            //}

            return debtCollections;
        }

        private async Task<bool> AddExpenseToIOU(Dictionary<int, decimal> debtIn, Dictionary<int, decimal> debtOut, Expense e, List<ExpenseAttendee> expenseAttendees)
        {
            // Eerst tellen we de kosten op voor degene die betaald heeft
            debtOut[e.PayerId] += e.Amount;

            int expenseDebtInCents = decimal.ToInt32(e.Amount * 100);

            // Ook zetten we 2 collecties met deelnemers klaar (we nemen bewust de percentage betalers nog samen!)
            List<ExpenseAttendee> setAmountPayers = new List<ExpenseAttendee>();
            List<ExpenseAttendee> percentagePayers = new List<ExpenseAttendee>();
            foreach (ExpenseAttendee exa in expenseAttendees)
            {
                Choice choice = await _context.Choices.FindAsync(exa.ChoiceId.Value);
                // HIER GOOIT IT EXCEPTION OBJECT REF NOT SET TO INSTANCE OF OBJECT!
                if (choice.ChoiceString.Contains("amount"))
                {
                    setAmountPayers.Add(exa);
                }
                if (choice.ChoiceString.Contains("percentage"))
                {
                    percentagePayers.Add(exa);
                }
            }

            // Vervolgens gaan we de kosten verdelen en aan de DebtIn collectie toevoegen, dit doen we in 3 stappen
            try
            {
                // Stap 1 de vaste bedragen FUNCTIE!!!
                foreach (ExpenseAttendee exa in setAmountPayers)
                {
                    expenseDebtInCents = AddSetAmountOfDebt(debtIn, expenseDebtInCents, exa);
                }

                // Stap 2 de vaste percentages FUNCTIE!!!
                int totalSetPercentageAmountInCents = 0;

                foreach (ExpenseAttendee exa in percentagePayers)
                {
                    if (!exa.Choice.ChoiceString.Contains("equal"))
                    {
                        if (!exa.OptionalSetPercentage.HasValue)
                        {
                            throw new Exception("A user has not set their percentage to pay for expense with id: " + exa.ExpenseId);
                        }
                        // Welk percentage wil deze persoon betalen?
                        int percentage = exa.OptionalSetPercentage.GetValueOrDefault();
                        AddPercentageOfDebt(debtIn, ref totalSetPercentageAmountInCents, expenseDebtInCents, exa, percentage);
                    }
                }

                expenseDebtInCents -= totalSetPercentageAmountInCents;

                // Stap 3 de "equal" percentages FUNCTIE!!!

                List<ExpenseAttendee> equalPercentagePayers = new List<ExpenseAttendee>();
                Dictionary<int, bool> attendeeBadluck = new Dictionary<int, bool>();
                foreach (ExpenseAttendee exa in percentagePayers)
                {
                    if (exa.Choice.ChoiceString.Contains("equal"))
                    {
                        equalPercentagePayers.Add(exa);
                        attendeeBadluck.Add(exa.AttendeeId, false);
                    }
                }

                // Here is where the magic happens, in plaats van een percentage uitrekenen verdelen we de restant van de schuld zo eerlijk mogelijk over de attendees die equal percentage willen.

                int numberOfEqualPercentagePayers = equalPercentagePayers.Count();
                int percentageAmountInCents = Math.DivRem(expenseDebtInCents, numberOfEqualPercentagePayers , out int leftovers);
                int totalEqualPercentageAmountInCents = 0;

                // Hier kan nog wat beters van gemaakt worden, maar voor nu eerst maar even deze gebruiken!
                if (leftovers != 0)
                {
                    Random rng = new Random();
                    for (int i = leftovers; i > 0; i--)
                    {
                        int badLuck = rng.Next(numberOfEqualPercentagePayers);
                        if (attendeeBadluck[badLuck] == false)
                        {
                            attendeeBadluck[badLuck] = true;
                        }
                        else
                        {
                            ++i;
                        }
                    }
                }

                foreach (ExpenseAttendee exa in equalPercentagePayers)
                {
                    totalEqualPercentageAmountInCents += percentageAmountInCents;
                    debtIn[exa.AttendeeId] += (decimal)percentageAmountInCents / 100M;
                    if (attendeeBadluck[exa.AttendeeId])
                    {
                        debtIn[exa.AttendeeId] += 0.01M;
                        totalEqualPercentageAmountInCents++;
                    }
                }

                expenseDebtInCents -= totalEqualPercentageAmountInCents;

                if (expenseDebtInCents != 0)
                {
                    throw new Exception("Sopmething went wrong while calculating, please contact back-end support!");
                }
                return true;
            }
            catch
            {
                throw;
            }
        }

        private static void AddPercentageOfDebt(Dictionary<int, decimal> debtIn, ref int totalAmount, int totalDebtInCents, ExpenseAttendee exa, int percentage)
        {
            // Here is where the magic happens !!!
            int percentageAmountInCents = Math.DivRem(totalDebtInCents * percentage, 100, out int hundredsCentRemainder);
            if (hundredsCentRemainder >= 50)
            {
                percentageAmountInCents++;
            }
            totalAmount += percentageAmountInCents;
            debtIn[exa.AttendeeId] += (decimal)percentageAmountInCents / 100M;
        }

        private int AddSetAmountOfDebt(Dictionary<int, decimal> debtIn, int totalDebtInCent, ExpenseAttendee exa)
        {
            if (!exa.OptionalSetAmount.HasValue)
            {
                throw new Exception("A user has not set their amount to pay for expense with id: " + exa.ExpenseId);
            }
            decimal value = exa.OptionalSetAmount.GetValueOrDefault();
            totalDebtInCent -= decimal.ToInt32(value * 100);
            debtIn[exa.AttendeeId] += value;
            return totalDebtInCent;
        }

        private void AddPayment(Dictionary<int, decimal> debtIn, Dictionary<int, decimal> debtOut, Payment p)
        {
            debtIn[p.ReceiverId.Value] += p.Amount;
            debtOut[p.PayerId.Value] += p.Amount;
        }

        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.Id == id);
        }

        private async Task<List<IOU>> CreateOptimizedIOUList(IDictionary<string, Dictionary<int, decimal>> debtCollections)
        {
            Dictionary<int, decimal> balanceCollection = new Dictionary<int, decimal>();

            Dictionary<int, decimal> debtIn = debtCollections["In"];
            Dictionary<int, decimal> debtOut = debtCollections["Out"];

            foreach (var attendeeId in debtCollections["In"].Keys)
            {
                decimal balance = debtCollections["In"][attendeeId] - debtCollections["Out"][attendeeId];
                balanceCollection.Add(attendeeId, balance);
            }

            List<IOU> result = new List<IOU>();

            while (balanceCollection.Any(entry => entry.Value != 0))
            {
                KeyValuePair<int, decimal> maxValue = balanceCollection.FirstOrDefault(entry => entry.Value == balanceCollection.Values.Max());
                KeyValuePair<int, decimal> minValue = balanceCollection.FirstOrDefault(entry => entry.Value == balanceCollection.Values.Min());

                var payer = await _context.Attendees.FirstOrDefaultAsync(a => a.Id == maxValue.Key);
                var receiver = await _context.Attendees.FirstOrDefaultAsync(a => a.Id == minValue.Key);

                IOU toAddIOU = new IOU(payer, receiver, 0);

                if (maxValue.Value <= Math.Abs(minValue.Value))
                {
                    toAddIOU.Amount = maxValue.Value;
                    balanceCollection[maxValue.Key] -= maxValue.Value;
                    balanceCollection[minValue.Key] += maxValue.Value;
                }
                else
                {
                    toAddIOU.Amount = minValue.Value;
                    balanceCollection[maxValue.Key] -= minValue.Value;
                    balanceCollection[minValue.Key] += minValue.Value;
                }

                result.Add(toAddIOU);
            }

            return result;
        }
    }
}
