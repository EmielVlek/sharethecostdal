﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/event")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;

        public EventsController(ShareTheCostsContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Get all the Events currently stored in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/event
        ///
        /// </remarks>
        /// <returns>All events</returns>
        /// <response code="200">Returns a collection of all events</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Event>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Event>>> GetEvents()
        {
            return await _context.Events.ToListAsync();
        }


        /// <summary>
        /// Get all the info on a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/event/1
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>One specific event</returns>
        /// <response code="200">Succesfully returns the requested event and all its details</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}")]
        [ProducesResponseType(typeof(Event), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Event>> GetEvent(int eventId)
        {
            Event @event = await _context.Events
                        .Include(e => e.EventAttendees).ThenInclude(eva => eva.Attendee)
                        .Include(e => e.Expenses)
                        .Include(e => e.Payments)
                        .FirstOrDefaultAsync(e => e.Id == eventId);


            if (@event == null)
            {
                return NotFound("Could not find the requested event in the database");
            }

            return @event;
        }

        /// <summary>
        /// Get all the expenses of a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/event/1/expenses
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>A collection of expenses of a specific event</returns>
        /// <response code="200">Succesfully returns the requested collection</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}/expenses")]
        [ProducesResponseType(typeof(IEnumerable<Expense>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Expense>>> GetEventExpenses(int eventId)
        {
            if (!EventExists(eventId))
            {
                return NotFound("Could not find the requested event in the database");
            }

            List<Expense> expenses = await _context.Expenses.Where(e => e.EventId == eventId).ToListAsync();

            return expenses;
        }


        /// <summary>
        /// Get all the payments of a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/event/1/payments
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>A collection of payments of a specific event</returns>
        /// <response code="200">Succesfully returns the requested collection</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}/payments")]
        [ProducesResponseType(typeof(IEnumerable<Payment>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Payment>>> GetEventPayments(int eventId)
        {
            if (!EventExists(eventId))
            {
                return NotFound("Could not find the requested event in the database");
            }

            List<Payment> payments = await _context.Payments.Where(e => e.EventId == eventId).ToListAsync();

            return payments;
        }


        /// <summary>
        /// Get all the attendees of a specific event
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/event/1/attendees
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>A collection of attendees of a specific event</returns>
        /// <response code="200">Succesfully returns the requested collection</response>
        /// <response code="404">If the requested event could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{eventId}/attendees")]
        [ProducesResponseType(typeof(IEnumerable<Attendee>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Attendee>>> GetEventAttendees(int eventId)
        {
            if (!EventExists(eventId))
            {
                return NotFound("Could not find the requested event in the database");
            }

            List<EventAttendee> eventAttendees = await _context.EventAttendees.Where(e => e.EventId == eventId).ToListAsync();

            List<Attendee> attendees = new List<Attendee>();
            if (eventAttendees.Count != 0)
            {
                foreach (var eva in eventAttendees)
                {
                    attendees.Add(_context.Attendees.Where(a => a.Id == eva.AttendeeId).FirstOrDefault());
                }
            }

            return attendees;
        }

        /// <summary>
        /// Replace a current event with a new name and/or new description and/ or attendees
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/v1/event/1
        ///     {
        ///         "eventName": "Example Name",
        ///         "userId": "user-id-string",
        ///         "eventDescription": "A new description for the event",
        ///         "id": 1,
        ///         "eventAttendees": [
        ///             {
        ///                 "choiceId": 2,
        ///                 "attendeeId": 1
        ///             },
        ///             {
        ///                 "attendeeId": 3
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <param name="eventId"></param>
        /// <param name="event"></param>
        /// <returns>Nothing if it was a succes</returns>
        /// <response code="204">Returns no content on a succesful PUT</response>
        /// <response code="400">Badrequest, check the body of your request</response>
        /// <response code="404">Could not find the event in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPut("{eventId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutEvent(int eventId, Event @event)
        {
            if (eventId != @event.Id)
            {
                return BadRequest("Id(event) given in body does not match your call");
            }
            Event storedEvent = await _context.Events.FindAsync(eventId);
            if (storedEvent == null)
            {
                return NotFound("Could not find the requested event in the database");
            }
            else if (storedEvent.UserId != @event.UserId)
            {
                return BadRequest("Event belongs to a different User!");
            }

            if (@event.EventName != null && @event.EventName != storedEvent.EventName)
            {
                storedEvent.EventName = @event.EventName;
            }

            if (@event.EventDescription != null && @event.EventDescription != storedEvent.EventDescription)
            {
                storedEvent.EventDescription = @event.EventDescription;
            }



            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new event with an initial set of attenees, if no new choice is given for an eventAttendee it will copy the attendee's default choice
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/event
        ///     {
        ///        "eventName": "name-of-event",
        ///        "userId": "d1b35d20-ceeb-4b24-a329-8beafb452506",
        ///        "eventAttendees": [
        ///             {
        ///                 "choiceId": 2,
        ///                 "attendeeId": 1
        ///             },
        ///             {
        ///                 "attendeeId": 3
        ///             }
        ///         ]
        ///     }
        ///     
        /// </remarks>
        /// <param name="newEvent">The new event</param>
        /// <returns>the created event</returns>
        /// <response code="201">Returns the newly created event</response>
        /// <response code="500">Server Error</response>
        [HttpPost]
        [ProducesResponseType(typeof(Event), 201)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Event>> PostEvent(Event newEvent)
        {
            try
            {
                _context.Events.Add(newEvent);
                foreach (var eva in newEvent.EventAttendees)
                {
                    if (eva.ChoiceId == 0)
                    {
                        eva.ChoiceId = _context.Attendees.Where(a => a.Id == eva.AttendeeId).FirstOrDefault().DefaultChoiceId;
                    }
                    _context.EventAttendees.Add(eva);
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                return StatusCode(500, e.Message);
            }

            return CreatedAtAction("GetEvent", new { eventId = newEvent.Id }, newEvent);
        }


        /// <summary>
        /// Add an Attendee to an existing event, if no choice given will add the default choice of the attendee
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/event/1/attendee
        ///     {
        ///        "choiceId": 2,
        ///        "attendeeId": 1
        ///     }
        ///     
        /// </remarks>
        /// <param name="eventId">The new event</param>
        /// <param name="eventAttendee">The new event</param>
        /// <returns>the created event</returns>
        /// <response code="201">Returns the newly created eventAttendee</response>
        /// <response code="400">Request was incomplete or attendeeId belongs to another user than the event</response>
        /// <response code="404">Could not find the event or attendee</response>
        /// <response code="500">Server Error</response>
        [HttpPost("{eventId}/attendee")]
        [ProducesResponseType(typeof(EventAttendee), 201)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<EventAttendee>> PostEventAttendee(int eventId, EventAttendee eventAttendee)
        {
            Event storedEvent = await _context.Events.FindAsync(eventId);
            if (storedEvent == null)
            {
                return NotFound("No event with this Id was found");
            }

            Attendee storedAttendee = await _context.Attendees.FindAsync(eventAttendee.AttendeeId);
            if (storedAttendee == null)
            {
                return NotFound("No attendee with this Id was found");
            }

            if (storedEvent.UserId != storedAttendee.UserId)
            {
                return BadRequest("Attendee and event do not belong to the same user");
            }

            eventAttendee.EventId = eventId;

            if (eventAttendee.ChoiceId == 0)
            {
                eventAttendee.ChoiceId = storedAttendee.DefaultChoiceId;
            }

            _context.EventAttendees.Add(eventAttendee);
            await _context.SaveChangesAsync();

            return new ActionResult<EventAttendee>(eventAttendee);
        }


        /// <summary>
        /// Remove an event from the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /api/v1/event/1
        ///     
        /// </remarks>
        /// <param name="eventId"></param>
        /// <returns>The deleted event</returns>
        /// <response code="200">Returns the deleted event</response>
        /// <response code="404">Could not find the event</response>
        /// <response code="500">Server Error</response>
        [HttpDelete("{eventId}")]
        [ProducesResponseType(typeof(Event), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Event>> DeleteEvent(int eventId)
        {
            Event @event = await _context.Events.FindAsync(eventId);
            if (@event == null)
            {
                return NotFound("Could not find the requested event in the database");
            }

            // TODO test of alles ook mee wordt deleted

            _context.Events.Remove(@event);

            await _context.SaveChangesAsync();

            return @event;
        }

        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.Id == id);
        }

    }
}
