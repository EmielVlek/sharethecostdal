﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/expense")]
    [ApiController]
    public class ExpensesController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;

        public ExpensesController(ShareTheCostsContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Get all the Expenses currently stored in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/expense
        ///
        /// </remarks>
        /// <returns>All expenses</returns>
        /// <response code="200">Returns a collection of all expenses</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Expense>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Expense>>> GetExpenses()
        {
            return await _context.Expenses.ToListAsync();
        }



        /// <summary>
        /// Get info on a specific expense
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/expense/1
        ///     
        /// </remarks>
        /// <param name="expenseId"></param>
        /// <returns>One specific attendee</returns>
        /// <response code="200">Succesfully returns the requested expense</response>
        /// <response code="404">If the requested expense could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{expenseId}")]
        [ProducesResponseType(typeof(Expense), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Expense>> GetExpense(int expenseId)
        {
            Expense expense = await _context.Expenses
                            .Include(ex => ex.ExpenseAttendees).ThenInclude(exa => exa.Attendee)
                            .FirstOrDefaultAsync(ex => ex.Id == expenseId);

            if (expense == null)
            {
                return NotFound("Could not find the requested expense");
            }

            return expense;
        }


        /// <summary>
        /// Get all the attendees of a specific expense
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/expense/1/attendees
        ///     
        /// </remarks>
        /// <param name="expenseId"></param>
        /// <returns>A collection of attendees of a specific expense</returns>
        /// <response code="200">Succesfully returns the requested collection of attendees</response>
        /// <response code="404">If the requested expense could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{expenseId}/attendees")]
        [ProducesResponseType(typeof(IEnumerable<ExpenseAttendee>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<ExpenseAttendee>>> GetExpenseAttendees(int expenseId)
        {
            if (!ExpenseExists(expenseId))
            {
                return NotFound("Could not find the requested expense");
            }

            List<ExpenseAttendee> expenseAttendees = await _context.ExpenseAttendees
                                                        .Where(exa => exa.ExpenseId == expenseId)
                                                        .Include(exa => exa.Attendee)
                                                        .ToListAsync();

            //List<Attendee> attendees = new List<Attendee>();
            //if (expenseAttendees.Count != 0)
            //{
            //    foreach (ExpenseAttendee exa in expenseAttendees)
            //    {
            //        attendees.Add(await _context.Attendees.Where(a => a.Id == exa.AttendeeId).FirstOrDefaultAsync());
            //    }
            //}

            return expenseAttendees;
        }




        /// <summary>
        /// Replace a current expense with a new ExpenseName and/or new Amount and/or new PurchaseDate and/or new DiscriptionWhere and/or new Payer and/or different attendees
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/v1/expenses/1
        ///     {
        ///         "expenseName": "Example Name",
        ///         "eventId": 2001,
        ///         "amount": 12.95,
        ///         "purchaseDate": "2020-01-01T00:00:00",
        ///         "payerId": 2,
        ///         "expenseAttendees": [
        ///             {
        ///                 "choiceId": 1,
        ///                 "optionalSetAmount": null,
        ///                 "optionalSetPercentage": null,
        ///                 "expenseId": 1,
        ///                 "attendeeId": 2,
        ///                 "id": 4
        ///             }
        ///         ],
        ///         "id": 1
        ///     }
        /// </remarks>
        /// <param name="expenseId"></param>
        /// <param name="expense"></param>
        /// <returns>Nothing if it was a succes</returns>
        /// <response code="204">Returns no content on a succesful PUT</response>
        /// <response code="400">Could not match the given expense</response>
        /// <response code="404">Could not find the expense in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPut("{expenseId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutExpense(int expenseId, Expense expense)
        {
            if (expenseId != expense.Id)
            {
                return BadRequest("Made a call to a different event than provided in the body");
            }

            Expense storedExpense = await _context.Expenses.FindAsync(expenseId);
            if (storedExpense == null)
            {
                return NotFound("Could not find the requested expense in the database");
            }

            if (expense.ExpenseName != null && expense.ExpenseName != storedExpense.ExpenseName)
            {
                storedExpense.ExpenseName = expense.ExpenseName;
            }

            if (expense.Amount != 0M && expense.Amount != storedExpense.Amount)
            {
                storedExpense.Amount = expense.Amount;
            }

            if ((expense.PurchaseDate != null && expense.PurchaseDate != DateTime.MinValue) && expense.PurchaseDate != storedExpense.PurchaseDate)
            {
                storedExpense.PurchaseDate = expense.PurchaseDate;
            }

            if (expense.DescriptionWhere != null && expense.DescriptionWhere != storedExpense.DescriptionWhere)
            {
                storedExpense.DescriptionWhere = expense.DescriptionWhere;
            }

            if (expense.PayerId != 0 && expense.PayerId != storedExpense.PayerId)
            {
                storedExpense.PayerId = expense.PayerId;
            }

            // TODO Zorgen dat ook alle info die van de Expense Attendees is aangepast : toevoegen attendee, aanpassen keuzes attendees etc etc. ook goed werk!

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }





        /// <summary>
        /// Create a new expense with an initial set of attenees, if no new choice is given it will copy the eventsAttendee's default choice
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/expense
        ///     {
        ///        "expenseName": "name-of-expense",
        ///        "amount": 123.45,
        ///        "descriptionWhere": "Here you can describe what kind of expense it is, groceries or rent etc..",
        ///        "purchaseDate": "2019-12-05",
        ///        "payerAttendeeId": 1,
        ///        "eventId": 4002,
        ///        "expenseAttendees": [
        ///             {
        ///                 "choiceId": 2,
        ///                 "attendeeId": 1
        ///             },
        ///             {
        ///                 "attendeeId": 3
        ///             }
        ///         ]
        ///     }
        ///     
        /// </remarks>
        /// <param name="expense">The new expense</param>
        /// <returns>the created expense</returns>
        /// <response code="201">Returns the newly created expense</response>
        /// <response code="400">One or more provided expenseAttendees are not part of the event and so they cannot be included into this expense</response>
        /// <response code="404">Could not find the event in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPost]
        [ProducesResponseType(typeof(Expense), 201)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Expense>> PostExpense(Expense expense)
        {
            Event @event = await _context.Events.Where(ev => ev.Id == expense.EventId).Include(ev => ev.EventAttendees).FirstOrDefaultAsync();
            if (@event == null)
            {
                return NotFound("Could not find the requested Event");
            }
            foreach (ExpenseAttendee exa in expense.ExpenseAttendees)
            {
                bool valid = false;
                foreach (EventAttendee eva in @event.EventAttendees)
                {
                    while (!valid)
                    {
                        if (exa.AttendeeId == eva.AttendeeId)
                        {
                            valid = true;
                        }
                    }
                }

                if (!valid)
                {
                    return BadRequest("Attendee is unknown to this event, so it cannot be added to this expense");
                }
            }
            
            _context.Expenses.Add(expense);

            foreach (var exa in expense.ExpenseAttendees)
            {
                if (exa.ChoiceId == 0 || exa.ChoiceId == null)
                {
                    exa.ChoiceId = _context.EventAttendees.Where(eva => eva.EventId == expense.EventId).FirstOrDefault().ChoiceId;
                }
                _context.ExpenseAttendees.Add(exa);
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return CreatedAtAction("GetExpense", new { expenseId = expense.Id }, expense);
        }





        /// <summary>
        /// Remove an expense from the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /api/v1/expense/1
        ///     
        /// </remarks>
        /// <param name="expenseId"></param>
        /// <returns>The deleted expense</returns>
        /// <response code="200">Returns the deleted expense</response>
        /// <response code="404">Could not find the expense</response>
        /// <response code="500">Server Error</response>
        [HttpDelete("{expenseId}")]
        [ProducesResponseType(typeof(Event), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Expense>> DeleteExpense(int expenseId)
        {
            if (!ExpenseExists(expenseId))
            {
                return NotFound("Could not find the expense");
            }

            var expenseAttendees = await _context.ExpenseAttendees
                                       .Where(ea => ea.ExpenseId == expenseId)
                                       .ToListAsync();
            foreach (var ea in expenseAttendees)
            {
                _context.ExpenseAttendees.Remove(ea);
            }

            var expense = await _context.Expenses.FindAsync(expenseId);
            _context.Expenses.Remove(expense);

            await _context.SaveChangesAsync();

            return expense;
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expenses.Any(e => e.Id == id);
        }
    }
}
