﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/user")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;
        private readonly UserMngr _userManager;

        public UsersController(ShareTheCostsContext context)
        {
            _context = context;
            _userManager = UserMngr.Create(context);
        }

        /// <summary>
        /// Get all the users currently stored in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/user
        ///
        /// </remarks>
        /// <returns>All users</returns>
        /// <response code="200">Returns a collection of all users</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<User>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _userManager.Users.ToListAsync();
        }



        /// <summary>
        /// Get info on a specific user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/user/1
        ///     
        /// </remarks>
        /// <param name="userId"></param>
        /// <returns>One specific attendee</returns>
        /// <response code="200">Succesfully returns the requested user</response>
        /// <response code="404">If the requested user could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{userId}")]
        [ProducesResponseType(typeof(User), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<User>> GetUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return NotFound("Could not find the requested user in the database");
            }

            return user;
        }



        /// <summary>
        /// Get all attendees linked to the requested user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/user/1/attendees
        ///     
        /// </remarks>
        /// <param name="userId"></param>
        /// <returns>One specific attendee</returns>
        /// <response code="200">Succesfully returns a collection of attendees linked to the requested user</response>
        /// <response code="404">If the requested user could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{userId}/attendees")]
        [ProducesResponseType(typeof(IEnumerable<Attendee>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Attendee>>> GetUserAttendees(string userId)
        {
            var user = await _userManager.Users.Where(u => u.Id == userId).Include(u => u.Attendees).FirstOrDefaultAsync();

            if (user == null)
            {
                return NotFound("Could not find the requested user in the database");
            }

            return new ActionResult<IEnumerable<Attendee>>(user.Attendees);
        }


        /// <summary>
        /// Get all events linked to the requested user
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/user/1/events
        ///     
        /// </remarks>
        /// <param name="userId"></param>
        /// <returns>One specific attendee</returns>
        /// <response code="200">Succesfully returns a collection of events linked to the requested user</response>
        /// <response code="404">If the requested user could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{userId}/events")]
        [ProducesResponseType(typeof(IEnumerable<Event>), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Event>>> GetUserEvents(string userId)
        {
            if (!UserExists(userId))
            {
                return NotFound("Could not find the requested user in the database");
            }

            return await _context.Events.Where(e => e.UserId == userId).ToListAsync();
        }


        /// <summary>
        /// Replace a current user with a new name and/or a new email // Use Patch to change passwords!
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/v1/users/1
        ///     {
        ///         "userId": "id-string-of-the-user",
        ///         "userName": "stringwithyournewname",
        ///         "email": "someemail@string.str"
        ///     }
        /// 
        /// </remarks>
        /// <param name="userId"></param>
        /// <param name="user"></param>
        /// <returns>Nothing if it was a succes</returns>
        /// <response code="204">Returns no content on a succesful PUT</response>
        /// <response code="400">Could not match the given user</response>
        /// <response code="404">Could not find a user in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPut("{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutUser(string userId, [FromBody]User user)
        {
            if (userId != user.Id)
            {
                return BadRequest("Mismatching user-id's given");
            }

            User storedUser = await _userManager.FindByIdAsync(userId);
            if (storedUser == null)
            {
                return NotFound("Could not find the requested user in the database");
            }

            storedUser.UserName = user.UserName;
            storedUser.Email = user.Email;
            
            await _userManager.UpdateAsync(storedUser);

            return NoContent();
        }


        /// <summary>
        /// Add a user to the database, if username is valid and email is unique
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /api/v1/user
        ///     {
        ///         "userName": "new User Name",
        ///         "email": "new@user.email",
        ///         "password": "non-hashed password string"
        ///     }
        /// 
        /// </remarks>
        /// <param name="info"></param>
        /// <returns>The created user</returns>
        /// <response code="201">Returns the newly created User</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Server Error</response>
        [HttpPost]
        [ProducesResponseType(typeof(User), 201)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<User>> PostUser(UserInfo info)
        {
            User newUser;
            try
            {
                newUser = new User { UserName = info.UserName, Email = info.Email };
                var result = await _userManager.CreateAsync(newUser, info.Password);
                if (!result.Succeeded)
                {
                    // TODO LOG results.Errors
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }

                    // TODO correct errors terug geven
                    return StatusCode(500, ModelState.ErrorCount);
                }
            }
            catch (DbUpdateException e)
            {
                return StatusCode(500, e.InnerException.Message);
            }

            var defaultChoice = await _context.Choices.FirstOrDefaultAsync();
            Attendee userAttendee = new Attendee(newUser.UserName, true, newUser, defaultChoice);

            // Dit zorgt dat ook de schaduw Attendee mee terug gegeven wordt aan de Client met de HTTPcode 201 (created ok)
            newUser.Attendees = new List<Attendee>
                {
                    userAttendee
                };

            _context.Attendees.Add(userAttendee);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { userId = newUser.Id }, newUser);
        }



        /// <summary>
        /// Still a work in progress :P Nog niet zomaar gebruiken!
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        public async Task<ActionResult<User>> DeleteUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            
            if (user == null)
            {
                return NotFound();
            }

            #region Remove related Attendees
            //var userAttendees = await _context.Attendees
            //                            .Where(a => a.UserId == id)
            //                            // .Where(a => a.IsUser == true) Snap even niet meer waarom deze hier nog staat, zo krijg ik volgens mij zombie Attendees
            //                            .ToListAsync();

            //foreach (var @attendee in userAttendees)
            //{
            //    _context.Attendees.Remove(@attendee);
            //}
            #endregion

            #region Remove related Events
            //var userEvents = await _context.Events
            //                        .Where(e => e.UserId == id)
            //                        .ToListAsync();

            //foreach (var @event in userEvents)
            //{
            //    var @eventAttendees = await _context.EventAttendees
            //                                    .Where(ea => ea.EventId == @event.Id)
            //                                    .ToListAsync();
            //    foreach (var ea in @eventAttendees)
            //    {
            //        _context.EventAttendees.Remove(ea);
            //    }

            //    var @expenses = await _context.Expenses
            //                                .Where(e => e.EventId == @event.Id)
            //                                .ToListAsync();
            //    foreach (var e in @expenses)
            //    {
            //        var @expenseAttendees = await _context.ExpenseAttendees
            //                                    .Where(ea => ea.ExpenseId == e.Id)
            //                                    .ToListAsync();
            //        foreach (var ea in @expenseAttendees)
            //        {
            //            _context.ExpenseAttendees.Remove(ea);
            //        }
            //        _context.Expenses.Remove(e);
            //    }

            //    var @payments = await _context.Payments
            //                                .Where(p => p.EventId == @event.Id)
            //                                .ToListAsync();
            //    foreach (var p in @payments)
            //    {
            //        _context.Payments.Remove(p);
            //    }

            //    _context.Events.Remove(@event);
            //}
            #endregion

            await _context.SaveChangesAsync();

            await _userManager.DeleteAsync(user);

            await _context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(string id)
        {
            return _userManager.Users.Any(e => e.Id == id);
        }
    }
}
