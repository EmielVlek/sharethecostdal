﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    [Route("api/v1/payment")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly ShareTheCostsContext _context;

        public PaymentsController(ShareTheCostsContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Get all the payments currently stored in the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/payment
        ///
        /// </remarks>
        /// <returns>All events</returns>
        /// <response code="200">Returns a collection of all payments</response>
        /// <response code="500">Server Error</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Payment>), 200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<IEnumerable<Payment>>> GetPayments()
        {
            return await _context.Payments.ToListAsync();
        }



        /// <summary>
        /// Get all the info on a specific payment
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /api/v1/payment/1
        ///     
        /// </remarks>
        /// <param name="paymentId"></param>
        /// <returns>One specific payment</returns>
        /// <response code="200">Succesfully returns the requested payment and all its details</response>
        /// <response code="404">If the requested payment could not be found</response>
        /// <response code="500">Server Error</response>
        [HttpGet("{paymentId}")]
        [ProducesResponseType(typeof(Payment), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Payment>> GetPayment(int paymentId)
        {
            var payment = await _context.Payments.FindAsync(paymentId);

            if (payment == null)
            {
                return NotFound();
            }

            return payment;
        }




        /// <summary>
        /// Replace a current payment with a new amount and/or new Amount and/ or attendees
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     PUT /api/v1/payment/1
        ///     {
        ///         "id": 1,
        ///         "amount": 12.34,
        ///         "remark": "Veld voor opmerking(en)",
        ///         "eventId": 2001,
        ///         "payerId": 1,
        ///         "receiverId": 2
        ///     }
        /// </remarks>
        /// <param name="paymentId"></param>
        /// <param name="payment"></param>
        /// <returns>Nothing if it was a succes</returns>
        /// <response code="204">Returns no content on a succesful PUT</response>
        /// <response code="400">Badrequest, check the body of your request</response>
        /// <response code="404">Could not find the event in database by given Id</response>
        /// <response code="500">Server Error</response>
        [HttpPut("{paymentId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> PutPayment(int paymentId, Payment payment)
        {
            if (paymentId != payment.Id)
            {
                return BadRequest("Could not match the given id to the call!");
            }
            Payment storedPayment = await _context.Payments.FindAsync(paymentId);
            if (storedPayment == null)
            {
                return NotFound("Could not find the requested payment int the database");
            }
            else if (storedPayment.EventId != payment.EventId)
            {
                return BadRequest("Payment belongs to a different Event!");
            }

            if (payment.Amount != 0 && payment.Amount != storedPayment.Amount)
            {
                storedPayment.Amount = payment.Amount;
            }

            if (payment.PayerId != null && payment.PayerId == storedPayment.PayerId)
            {
                storedPayment.PayerId = payment.PayerId;
            }

            if (payment.ReceiverId != null && payment.ReceiverId == storedPayment.ReceiverId)
            {
                storedPayment.ReceiverId = payment.ReceiverId;
            }

            if (payment.Remark != null && payment.Remark == storedPayment.Remark)
            {
                storedPayment.Remark = payment.Remark;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentExists(paymentId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }




        /// <summary>
        /// Create a new payment
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/v1/payment
        ///     {
        ///         "amount": 12.34,
        ///         "remark": "Veld voor een opmerking, kan ook leeg blijven",
        ///         "eventId": 2001,
        ///         "payerId": 1,
        ///         "receiverId": 2
        ///     }
        ///     
        /// </remarks>
        /// <param name="newPayment">The new payment</param>
        /// <returns>the created payment</returns>
        /// <response code="201">Returns the newly created payment</response>
        /// <response code="500">Server Error</response>
        [HttpPost]
        [ProducesResponseType(typeof(Event), 201)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Payment>> PostPayment(Payment newPayment)
        {
            try
            {
                _context.Payments.Add(newPayment);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                return StatusCode(500, e.Message);
            }

            return CreatedAtAction("GetPayment", new { paymentId = newPayment.Id }, newPayment);
        }



        /// <summary>
        /// Remove a payment from the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     DELETE /api/v1/payments/1
        ///     
        /// </remarks>
        /// <param name="paymentId"></param>
        /// <returns>The deleted payment</returns>
        /// <response code="200">Returns the deleted payment</response>
        /// <response code="404">Could not find the payment</response>
        /// <response code="500">Server Error</response>
        [HttpDelete("{paymentId}")]
        [ProducesResponseType(typeof(Payment), 200)]
        [ProducesResponseType(typeof(IDictionary<string, string>), 404)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<Payment>> DeletePayment(int paymentId)
        {
            if (PaymentExists(paymentId))
            {
                return NotFound();
            }

            var payment = await _context.Payments.FindAsync(paymentId);
            _context.Payments.Remove(payment);
            await _context.SaveChangesAsync();
            return payment;
        }

        private bool PaymentExists(int id)
        {
            return _context.Payments.Any(e => e.Id == id);
        }
    }
}
