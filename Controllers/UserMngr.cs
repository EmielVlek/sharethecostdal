﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ShareTheCosts.Data;
using ShareTheCosts.Models;

namespace ShareTheCosts.Controllers
{
    public class UserMngr : UserManager<User>
    {
        public UserMngr(IUserStore<User> store, IPasswordHasher<User> hasher) : base(store, null, hasher, null, null, null, null, null, null)
        {
        }

        public static UserMngr Create(ShareTheCostsContext context)
        {
            UserMngr manager = new UserMngr(
                new UserStore<User>(context), new PasswordHasher<User>());
            return manager;
        }
    }
}
